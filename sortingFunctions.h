/************************
	Muris Fazlic
************************/
#ifndef INC_SORT_H
#define INC_SORT_H

void createRandomArray(int, const char*);
void createSortedArray(int, const char*);
void createSortedArray_2(const char*);
void createSemi(int, const char*);
void createSemi_2(const char*);
void populateArray(int [], const char*, int);
void insertionSort(int [], int);
void selectionSort(int [], int);
void bubbleSort(int [], int);
void mergeSort(int [], int, int);
void merge(int [], int [], int, int, int);
void heapSort(int [], int);
void heapify(int [], int, int);
void buildHeap(int [], int);
void quickSort(int [], int, int);
int partition(int [], int, int);
#endif
