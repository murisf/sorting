/****************************************
	Muris Fazlic

	Sorting algorithms are coded so as
	not to sort the first element A[0]
	as this will be a sentinal value
****************************************/
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include "sortingFunctions.h"

using namespace std;

void createRandomArray(int length, const char* filename){
	ofstream outputFile(filename);
	srand(time(NULL));

	outputFile << "0" << endl;

	for(int i = 0;i < length;i++)
		outputFile << rand() % 1000 + 1 << endl;

	outputFile.close();
}

void createSortedArray(int length, const char* filename){
	ofstream outputFile(filename);

	outputFile << "0" << endl;

	for (int i = 1;i <= length;i++)
		outputFile << i << endl;
	
	outputFile.close();
}

void createSortedArray_2(const char* filename){
	ofstream outputFile(filename);

	outputFile << "0" << endl;

	for (int i = 1;i <=1000;i++){
		for (int j = 0;j < 10;j++)
			outputFile << i << endl;
	}

	outputFile.close();
}

void createSemi(int length, const char* filename){
	ofstream outputFile(filename);
	srand(time(NULL));

	outputFile << "0" << endl;

	for (int i = 1;i <= length;i++){
		if(i%10 == 0){
			outputFile << rand() % 1000 + 1 << endl;
			continue;
		}
		outputFile << i << endl;
	}
	outputFile.close();
}

void createSemi_2(const char* filename){
	ofstream outputFile(filename);
	srand(time(NULL));

	outputFile << "0" << endl;

	for (int i = 1;i <= 1000;i++){
		for(int j = 0;j < 10;j++){
			if(i%10 == 0){
				outputFile << rand() % 1000 + 1 << endl;
				continue;
			}
			outputFile << i << endl;
		}
	}
		
	outputFile.close();
}

void populateArray(int A[], const char* filename, int length){
	ifstream inputFile(filename);

	for (int i = 0;i < length;i++)
		inputFile >> A[i];

	inputFile.close();
}

void insertionSort(int A[], int length){
	int key, i;

	for(int j = 2;j <= length;j++){
		key = A[j];
		i = j-1;
		while (i >= 1 && A[i] > key){
			A[i+1] = A[i];
			i = i-1;
		}
		A[i+1] = key;
	}
}

void selectionSort(int A[], int length){
	int min, temp;
		
	for(int i = 1;i <= length-1;i++){
		min = i;
							
		for(int j = i+1;j <= length;j++){
			if (A[j] < A[min])
				min = j;
		}
		temp = A[i];
		A[i] = A[min];
		A[min] = temp;
	}
}

void bubbleSort(int A[], int length){
	int temp;
		
	for(int i = 1;i <= length-1;i++){
		for(int j = 1;j <= length-i;j++){
			if (A[j+1] < A[j]){
				temp = A[j+1];
				A[j+1] = A[j];
				A[j] = temp;
			}
		}
	}
}

void mergeSort(int A[], int low, int high){
	int mid, B[high];

	if (low < high) {
		mid = (low+high)/2;
		mergeSort(A, low, mid);
		mergeSort(A, mid+1, high);
		merge(A, B, low, mid, high);
	}
}

void merge(int A[], int B[], int low, int mid, int high){
	int k;
	int h = low, i = low, j = mid+1;
	while ((h <= mid) && (j <= high)){
		if (A[h] <= A[j]){
			B[i] = A[h];
			h++;
		}
		else {
			B[i] = A[j];
			j++;
		}
		i++;
	}
	if (h > mid) {
		for(k = j;k <= high;k++){
			B[i] = A[k];
			i++;
		}
	}
	else {
		for(k = h;k<=mid;k++){
			B[i] = A[k];
			i++;
		}
	}
	for(k = low;k <= high;k++)
		A[k] = B[k];
}

void heapSort(int A[], int length){
	int i, temp;

	buildHeap(A, length);

	for(i = length;i >= 2;i--){
		temp = A[i];
		A[i] = A[1];
		A[1] = temp;
		heapify(A, 1, i-1);
	}
}

void heapify(int A[], int i, int length){
	int j, temp;
	temp = A[i];
	j = 2*i;

	while (j <= length){
		if (j < length && A[j+1] > A[j])
			j = j+1;
		if (temp > A[j])
			break;
		else if (temp <= A[j]){
			A[j/2] = A[j];
			j = 2*j;
		}
	}
	A[j/2] = temp;

	return;
}

void buildHeap(int A[], int length){
	for(int i = length/2;i >= 1; i--){
		heapify(A, i, length);
	}
}

void quickSort(int A[], int p, int r){
	if (p >= r)
		return;
	
	int q = partition(A, p, r);

	quickSort(A, p, q-1);
	quickSort(A, q+1, r);
}

int partition(int A[], int p, int r){
	int x = A[r];
	int i = p-1;
	
	for(int j = p;j <= r-1;j++){
		if (A[j] <= x){
			i = i+1;
			int temp = A[i];
			A[i] = A[j];
			A[j] = temp;
		}
	}

	int temp = A[i+1];
	A[i+1] = A[r];
	A[r] = temp;

	return i + 1;
}